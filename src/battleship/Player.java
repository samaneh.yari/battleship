package battleship;

import java.util.ArrayList;
import java.util.Random;

enum attackType {Regular, Faulted}
enum playerType {User, Computer}

public class Player {
    private static int SHIPS_COUNT = 5;
    private Ship[] ships;
    private attackType aType;
    private ArrayList<Point> successHits;
    private ArrayList<Point> badHits;
    private ArrayList<Point> damages;
    private int livesLeft;
    private playerType pType;

    public playerType getpType() {
        return pType;
    }

    public static int getShipsCount() {
        return SHIPS_COUNT;
    }

    public Ship ShipAt(Point p) {
        for (int i = 0; i < Player.getShipsCount(); i++) {
            if (ships[i] != null)
                if (ships[i].getPosition().isPointInPosition(p))
                    return ships[i];
        }
        return null;
    }

    public void AddShip(Ship ship) {
        this.ships[ship.getId()] = ship;
        livesLeft += ship.getSize();
    }

    public Player(attackType atype, playerType ptype) {
        this.ships = new Ship[SHIPS_COUNT];
        this.aType = atype;
        this.pType = ptype;
        livesLeft = 0;
        successHits = new ArrayList<>();
        badHits = new ArrayList<>();
        damages = new ArrayList<>();
    }

    public void hitted(Point point) {
        livesLeft--;
        damages.add(point);
    }

    public boolean isDamaged(Point point) {
        for (int i = 0; i < damages.size(); i++) {
            if (damages.get(i).equals(point)) return true;
        }
        return false;
    }

    public boolean isSuccessHit(Point point) {
        for (int i = 0; i < successHits.size(); i++) {
            if (successHits.get(i).equals(point)) return true;
        }
        return false;
    }

    public boolean isBadHit(Point point) {
        for (int i = 0; i < badHits.size(); i++) {
            if (badHits.get(i).equals(point)) return true;
        }
        return false;
    }

    public boolean lost() {
        return livesLeft == 0;
    }

    public boolean attack(Point point, Player enemy) {
        Point attackPoint = point;
        if (aType == attackType.Faulted) {
            Random random = new Random();
            ArrayList<Point> nearbys = point.getNearBys();
            int i = 0;
            while (i < nearbys.size()) {
                if (isSuccessHit(nearbys.get(i)) || isBadHit(nearbys.get(i))) {
                    nearbys.remove(i);
                    i--;
                }
                i++;
            }
            if (nearbys.size() == 0) return false;
            int rand = random.nextInt(nearbys.size());
            attackPoint = nearbys.get(rand);
        }

        if (isSuccessHit(attackPoint) || isBadHit(attackPoint)) return false;
        if (enemy.ShipAt(attackPoint) != null) {
            successHits.add(attackPoint);
            enemy.hitted(attackPoint);
        } else {
            badHits.add(attackPoint);
        }
        return true;
    }

}
