package battleship;

import java.util.ArrayList;

public class Point {
    private int x;
    private int y;
    ArrayList<Point> nearbys;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point(int x, int y) {
        if (x >= 0 && y >= 0) {
            this.x = x;
            this.y = y;
        }
        // TODO: throw exception here
        nearbys = new ArrayList<>();
    }

    private void findNearBys() {
        if (this.equals(new Point(0, 0))) {
            nearbys.add(new Point(0, 0));
            nearbys.add(new Point(0, 1));
            nearbys.add(new Point(1, 0));
            nearbys.add(new Point(1, 1));
        } else if (this.equals(new Point(9, 0))) {
            nearbys.add(new Point(9, 0));
            nearbys.add(new Point(8, 0));
            nearbys.add(new Point(8, 1));
            nearbys.add(new Point(9, 1));
        } else if (this.equals(new Point(0, 9))) {
            nearbys.add(new Point(0, 9));
            nearbys.add(new Point(0, 8));
            nearbys.add(new Point(1, 9));
            nearbys.add(new Point(1, 8));
        } else if (this.equals(new Point(9, 9))) {
            nearbys.add(new Point(9, 9));
            nearbys.add(new Point(9, 8));
            nearbys.add(new Point(8, 9));
            nearbys.add(new Point(8, 8));
        } else if (this.getX() == 0) {
            nearbys.add(new Point(0, this.getY() - 1));
            nearbys.add(new Point(0, this.getY()));
            nearbys.add(new Point(0, this.getY() + 1));
            nearbys.add(new Point(1, this.getY() - 1));
            nearbys.add(new Point(1, this.getY()));
            nearbys.add(new Point(1, this.getY() + 1));
        } else if (this.getX() == 9) {
            nearbys.add(new Point(9, this.getY() - 1));
            nearbys.add(new Point(9, this.getY()));
            nearbys.add(new Point(9, this.getY() + 1));
            nearbys.add(new Point(8, this.getY() - 1));
            nearbys.add(new Point(8, this.getY()));
            nearbys.add(new Point(8, this.getY() + 1));
        } else if (this.getY() == 0) {
            nearbys.add(new Point(this.getX() - 1, 0));
            nearbys.add(new Point(this.getX(), 0));
            nearbys.add(new Point(this.getX() + 1, 0));
            nearbys.add(new Point(this.getX() - 1, 1));
            nearbys.add(new Point(this.getX(), 1));
            nearbys.add(new Point(this.getX() + 1, 1));
        } else if (this.getY() == 9) {
            nearbys.add(new Point(this.getX() - 1, 9));
            nearbys.add(new Point(this.getX(), 9));
            nearbys.add(new Point(this.getX() + 1, 9));
            nearbys.add(new Point(this.getX() - 1, 8));
            nearbys.add(new Point(this.getX(), 8));
            nearbys.add(new Point(this.getX() + 1, 8));
        } else {
            nearbys.add(new Point(this.getX() - 1, this.getY() - 1));
            nearbys.add(new Point(this.getX(), this.getY() - 1));
            nearbys.add(new Point(this.getX() + 1, this.getY() - 1));
            nearbys.add(new Point(this.getX() - 1, this.getY()));
            nearbys.add(new Point(this.getX(), this.getY()));
            nearbys.add(new Point(this.getX() + 1, this.getY()));
            nearbys.add(new Point(this.getX() - 1, this.getY() + 1));
            nearbys.add(new Point(this.getX(), this.getY() + 1));
            nearbys.add(new Point(this.getX() + 1, this.getY() + 1));
        }
    }

    public ArrayList<Point> getNearBys() {
        if (nearbys.size() == 0) findNearBys();
        return nearbys;
    }

    public boolean equals(Point o) {
        return this.getX() == o.getX() && this.getY() == o.getY();
    }
}
