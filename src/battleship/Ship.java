package battleship;

public class Ship {
    private int id;
    private int size;
    private Position position;

    public int getId() {
        return id;
    }

    public int getSize() {
        return size;
    }

    public Position getPosition() {
        return position;
    }

    public Ship(int id, Position position) {
        this.id = id;
        this.position = position;
        this.size = position.getSize();
    }
}
