package battleship;

public class Board {
    private Player p1, p2;

    public Board(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void PrintBoard(int playerNumber) {
        System.out.println();
        System.out.println("Player (1)                                     Player (2)");
        System.out.println();
        System.out.print("  | ");
        for (int i = 0; i < 10; i++)
            System.out.print(i + " | ");
        System.out.print("   ");
        System.out.print("  | ");
        for (int i = 0; i < 10; i++)
            System.out.print(i + " | ");
        System.out.println();

        System.out.print("--|");
        for (int i = 0; i < 10; i++)
            System.out.print("---+");
        System.out.print("    ");
        System.out.print("--|");
        for (int i = 0; i < 10; i++)
            System.out.print("---+");
        System.out.println("");

        for (int row = 0; row < 10; row++) {
            if (playerNumber == 1) {
                // player board
                System.out.print(row + " |");
                for (int col = 0; col < 10; col++) {
                    System.out.print(" ");

                    boolean isShip = false;
                    for (int i = 0; i < Player.getShipsCount(); i++) {
                        Ship s = p1.ShipAt(new Point(col, row));
                        if (s != null) {
                            isShip = true;
                            if (p1.isDamaged(new Point(col, row)))
                                System.out.print("#");
                            else
                                System.out.print("@");

                            break;
                        }
                    }
                    if (!isShip)
                        System.out.print(" ");

                    System.out.print(" |");
                }

                System.out.print("    ");

                // enemy board
                System.out.print(row + " |");
                for (int col = 0; col < 10; col++) {
                    System.out.print(" ");

                    for (int i = 0; i < Player.getShipsCount(); i++) {
                        if (p1.isSuccessHit(new Point(col, row))) {
                            System.out.print("&");
                            break;
                        } else if (p1.isBadHit(new Point(col, row))) {
                            System.out.print("X");
                            break;
                        } else if (i == Player.getShipsCount() - 1) {
                            System.out.print(" ");
                            break;
                        }
                    }

                    System.out.print(" |");
                }

                System.out.println("");
                System.out.print("--|");
                for (int i = 0; i < 10; i++)
                    System.out.print("---+");
                System.out.print("    ");
                System.out.print("--|");
                for (int i = 0; i < 10; i++)
                    System.out.print("---+");
                System.out.println("");
            } else {
                // enemy board
                System.out.print(row + " |");
                for (int col = 0; col < 10; col++) {
                    System.out.print(" ");

                    for (int i = 0; i < Player.getShipsCount(); i++) {
                        if (p2.isSuccessHit(new Point(col, row))) {
                            System.out.print("&");
                            break;
                        } else if (p2.isBadHit(new Point(col, row))) {
                            System.out.print("X");
                            break;
                        } else if (i == Player.getShipsCount() - 1) {
                            System.out.print(" ");
                            break;
                        }
                    }

                    System.out.print(" |");
                }

                System.out.print("    ");

                // player board
                System.out.print(row + " |");
                for (int col = 0; col < 10; col++) {
                    System.out.print(" ");

                    boolean isShip = false;
                    for (int i = 0; i < Player.getShipsCount(); i++) {
                        Ship s = p2.ShipAt(new Point(col, row));
                        if (s != null) {
                            isShip = true;
                            if (p2.isDamaged(new Point(col, row)))
                                System.out.print("#");
                            else
                                System.out.print("@");

                            break;
                        }
                    }
                    if (!isShip)
                        System.out.print(" ");

                    System.out.print(" |");
                }

                System.out.println("");
                System.out.print("--|");
                for (int i = 0; i < 10; i++)
                    System.out.print("---+");
                System.out.print("    ");
                System.out.print("--|");
                for (int i = 0; i < 10; i++)
                    System.out.print("---+");
                System.out.println("");
            }

        }
    }
}
