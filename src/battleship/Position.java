package battleship;

public class Position {
    private Point p1;
    private Point p2;

    public Position(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public int getSize() {
        if (p1.getY() == p2.getY()) return Math.abs(p1.getX() - p2.getX()) + 1;
        else return Math.abs(p1.getY() - p2.getY()) + 1;
    }

    public boolean isPointInPosition(Point p) {
        if (p.getY() == p1.getY() && p.getX() <= p2.getX() && p.getX() >= p1.getX())
            return true;
        else if (p.getY() == p1.getY() && p.getX() <= p1.getX() && p.getX() >= p2.getX())
            return true;
        else if (p.getX() == p1.getX() && p.getY() <= p2.getY() && p.getY() >= p1.getY())
            return true;
        else if (p.getX() == p1.getX() && p.getY() <= p1.getY() && p.getY() >= p2.getY())
            return true;

        return false;
    }
}
