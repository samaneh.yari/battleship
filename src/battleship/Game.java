package battleship;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private Player p1, p2;
    private Board board;

    public Game() {
        placeShips();
    }

    private void placeShips() {
        Scanner sc = new Scanner(System.in);
        int pType = 1;
        for (int p = 1; p < 3; p++) {
            if (p == 1) {
                System.out.println("Player " + p + " (User)");
            } else {
                System.out.print("Player " + p + " (1=User, 2=Computer): ");
                pType = sc.nextInt();
            }
            int attackType;
            System.out.print("What is your attack type(Regular = 1, Faulted = 2): ");

            if (p == 1) {
                attackType = sc.nextInt();
                if (attackType == 1) {
                    p1 = new Player(battleship.attackType.Regular, playerType.User);
                }
                else {
                    p1 = new Player(battleship.attackType.Faulted, playerType.User);
                }
            } else {
                if (pType == 1) {
                    attackType = sc.nextInt();
                    if (attackType == 1)
                        p2 = new Player(battleship.attackType.Regular, playerType.User);
                    else
                        p2 = new Player(battleship.attackType.Faulted, playerType.User);
                } else {
                    Random random = new Random();
                    attackType = random.nextInt(2) + 1;
                    System.out.println(attackType);
                    if (attackType == 1)
                        p2 = new Player(battleship.attackType.Regular, playerType.Computer);
                    else
                        p2 = new Player(battleship.attackType.Faulted, playerType.Computer);
                }
            }

            for (int i = 0; i < 5; i++) {
                System.out.print("Enter ship " + (i + 1) + " coordinates(x1, y1, x2, y2): ");

                if (p == 1) {
                    int x1 = sc.nextInt();
                    int y1 = sc.nextInt();
                    int x2 = sc.nextInt();
                    int y2 = sc.nextInt();
                    p1.AddShip(new Ship(i, new Position(new Point(x1, y1), new Point(x2, y2))));
                } else {
                    int x1, y1, x2, y2;
                    if (p2.getpType() == playerType.User) {
                        x1 = sc.nextInt();
                        y1 = sc.nextInt();
                        x2 = sc.nextInt();
                        y2 = sc.nextInt();
                    } else {
                        Random random = new Random();
                        boolean vertical = random.nextBoolean();
                        x1 = random.nextInt(10);
                        y1 = random.nextInt(10);
                        if (vertical) {
                            int rSize = random.nextInt(5) + 1;
                            x2 = x1;
                            if(y1 < rSize)
                                y2 = random.nextInt(rSize - y1) + rSize - 1;
                            else if(y1 > rSize)
                                y2 = rSize - random.nextInt(y1 - rSize) - 2;
                            else
                                y2 = rSize + y1 - 1;
                        } else {
                            int rSize = random.nextInt(5) + 1;
                            if(x1 < rSize)
                                x2 = random.nextInt(rSize - x1) + rSize - 1;
                            else if(x1 > rSize)
                                x2 = rSize - random.nextInt(x1 - rSize) - 2;
                            else
                                x2 = rSize + x1 - 1;
                            y2 = y1;
                        }
                        System.out.println(x1 + " " + y1 + " " + x2 + " " + y2);
                    }
                    p2.AddShip(new Ship(i, new Position(new Point(x1, y1), new Point(x2, y2))));
                }
            }
        }
        board = new Board(p1, p2);
    }


    public void start() {
        Scanner sc = new Scanner(System.in);
        boolean p1Turn = true;
        while (true) {
            System.out.println();
            if (p1Turn) {
                board.PrintBoard(1);
                System.out.print("\nPlayer " + "1" + " Enter attack coordinate(x, y): ");
            } else {
                board.PrintBoard(2);
                System.out.print("\nPlayer " + "2" + " Enter attack coordinate(x, y): ");
            }

            int x, y;
            if (!p1Turn) {
                if (p2.getpType() == playerType.Computer) {
                    Random random = new Random();
                    x = random.nextInt(10);
                    y = random.nextInt(10);
                    System.out.println(x + " " + y);
                } else {
                    x = sc.nextInt();
                    y = sc.nextInt();
                }
            } else {
                x = sc.nextInt();
                y = sc.nextInt();
            }

            if (p1Turn) {
                boolean validAttackPoint = p1.attack(new Point(x, y), p2);
                if (!validAttackPoint) {
                    System.out.println("Enter a valid point!");
                    continue;
                }
                board.PrintBoard(1);
                p1Turn = false;
            } else {
                boolean validAttackPoint = p2.attack(new Point(x, y), p1);
                if (!validAttackPoint) {
                    System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    System.out.println("XXXXX Enter a valid point! XXXXX");
                    System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    continue;
                }
                board.PrintBoard(2);
                p1Turn = true;
            }


            if (p1.lost()) {
                System.out.println("********************************");
                System.out.println("****Player (2) won the game!****");
                System.out.println("********************************");
                break;
            }
            if (p2.lost()) {
                System.out.println("********************************");
                System.out.println("****Player (1) won the game!****");
                System.out.println("********************************");
                break;
            }
        }

        System.out.println();
    }
}
